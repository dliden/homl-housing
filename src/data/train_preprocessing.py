import numpy as np
from pathlib import Path
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import OneHotEncoder
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
import logging
from dotenv import find_dotenv, load_dotenv
from make_dataset import load_housing_data

PROJECT_ROOT = Path.cwd()
RAW_PATH = PROJECT_ROOT.joinpath('data').joinpath('raw')
INTERIM_PATH = PROJECT_ROOT.joinpath('data').joinpath('interim')
FINAL_PATH = PROJECT_ROOT.joinpath('data').joinpath('processed')


def main():
    INTERIM_PATH.mkdir(parents=True, exist_ok=True)
    RAW_PATH.mkdir(exist_ok=True)
    FINAL_PATH.mkdir(exist_ok=True)
    train_data = load_housing_data(FINAL_PATH.joinpath('train_set_strat.csv'))

    train_labels = train_data["median_house_value"].copy()
    train_unlabeled = train_data.drop("median_house_value", axis=1).\
        drop("Unnamed: 0", axis=1).drop("Unnamed: 0.1", axis=1)
    train_num = train_unlabeled.drop("ocean_proximity", axis=1)

    num_attributes = list(train_num)
    cat_attributes = ["ocean_proximity"]

    num_pipeline = Pipeline([
        ('imputer', SimpleImputer(strategy="median")),
        ('attribs_adder', CombinedAttributesAdder()),
        ('std_scaler', StandardScaler()),
        ])

    full_pipeline = ColumnTransformer([
        ("num", num_pipeline, num_attributes),
        ("cat", OneHotEncoder(), cat_attributes),
    ])

    train_processed = full_pipeline.fit_transform(train_data)
    np.savetxt(FINAL_PATH.joinpath('train_processed.csv'), train_processed,
               delimiter=", ")
    train_labels.to_csv(FINAL_PATH.joinpath('train_processed_labels.csv'),
                        header=False)


categorical_var_encoder = OneHotEncoder()


class CombinedAttributesAdder(BaseEstimator, TransformerMixin):
    rooms_ix, bedrooms_ix, population_ix, households_ix = 3, 4, 5, 6

    def __init__(self, add_bedrooms_per_room=True):  # no *args or **kargs
        self.add_bedrooms_per_room = add_bedrooms_per_room

    def fit(self, X, y=None):
        return self  # nothing else to do

    def transform(self, X, y=None):
        rooms_per_household = X[:, self.rooms_ix] / X[:, self.households_ix]
        population_per_household = X[:, self.population_ix] /\
            X[:, self.households_ix]
        if self.add_bedrooms_per_room:
            bedrooms_per_room = X[:, self.bedrooms_ix] / X[:, self.rooms_ix]
            return np.c_[X, rooms_per_household,
                         population_per_household, bedrooms_per_room]
        else:
            return np.c_[X, rooms_per_household, population_per_household]


num_pipeline = Pipeline([
    ('imputer', SimpleImputer(strategy="median")),
    ('attribs_adder', CombinedAttributesAdder()),
    ('std_scaler', StandardScaler()),
])


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
