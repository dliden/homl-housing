# -*- coding: utf-8 -*-
# import click
import logging
import numpy as np
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
from six.moves import urllib
import tarfile
import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit


# @click.command()
# @click.argument('input_filepath', type=click.Path(exists=True))
# @click.argument('output_filepath', type=click.Path())
# def main(input_filepath, output_filepath):
#    """ Runs data processing scripts to turn raw data from (../raw) into
#        cleaned data ready to be analyzed (saved in ../processed).
#    """


DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml2/master/"
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"
PROJECT_ROOT = Path.cwd()
RAW_PATH = PROJECT_ROOT.joinpath('data').joinpath('raw')
INTERIM_PATH = PROJECT_ROOT.joinpath('data').joinpath('interim')
FINAL_PATH = PROJECT_ROOT.joinpath('data').joinpath('processed')


def main():
    INTERIM_PATH.mkdir(parents=True, exist_ok=True)
    RAW_PATH.mkdir(exist_ok=True)
    FINAL_PATH.mkdir(exist_ok=True)
    fetch_housing_data()
    stratify_housing_data()
    train_set, test_set = split_stratified_housing_train_test()
    train_set.to_csv(FINAL_PATH.joinpath('train_set_strat.csv'))
    test_set.to_csv(FINAL_PATH.joinpath('test_set_strat.csv'))


def fetch_housing_data(housing_url=HOUSING_URL, housing_path=RAW_PATH):
    tgz_path = housing_path.joinpath('housing.tgz')
    urllib.request.urlretrieve(housing_url, tgz_path)
    housing_tgz = tarfile.open(tgz_path)
    housing_tgz.extractall(path=INTERIM_PATH)
    housing_tgz.close()


def load_housing_data(housing_path=INTERIM_PATH.joinpath('housing.csv')):
    logger = logging.getLogger(__name__)
    logger.info('loading data from specified directory')
    return pd.read_csv(housing_path, engine='python')


def stratify_housing_data():
    housing = load_housing_data()
    housing["income_cat"] = pd.cut(housing["median_income"],
                                   bins=[0., 1.5, 3.0, 4.5, 6, np.inf],
                                   labels=[1, 2, 3, 4, 5])
    housing.to_csv(INTERIM_PATH.joinpath('housing_stratified.csv'))


def split_stratified_housing_train_test():
    stratified_housing_data = load_housing_data(INTERIM_PATH.joinpath\
                                                ('housing_stratified.csv'))
    split = StratifiedShuffleSplit(n_splits=1, test_size=0.2, random_state=42)
    for train_index, test_index in split.split\
            (stratified_housing_data, stratified_housing_data["income_cat"]):
        strat_train_set = stratified_housing_data.loc[train_index]
        strat_test_set = stratified_housing_data.loc[test_index]
        for set_ in (strat_train_set, strat_test_set):
            set_.drop("income_cat", axis=1, inplace=True)
        return (strat_train_set, strat_test_set)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    main()
