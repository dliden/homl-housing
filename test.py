#import src.data
from src.data import make_dataset

# Load Data

make_dataset.fetch_housing_data()

data = make_dataset.load_housing_data()
