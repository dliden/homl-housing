from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Housing price example from "Hands On Machine Learning"',
    author='Daniel Liden',
    license='',
)
